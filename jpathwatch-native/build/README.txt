This folder contains the 'native' subfolder hierarchy, which in turn should contain all native libraries required for FileWatch.

The whole 'native' folder hierarchy is incorporated into the final .jar file by the NetBeans build process, so before actually building the JAR file, all native libraries must be present in this directory structure.